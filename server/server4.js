/**
 * A friendly server that responds to URL requests with plain text after calling another service
 * Try: localhost:8084/Maryville, Missouri
 */
const http = require("http")
const temperatureService = require('./TemperatureService.js')
const port = 8084

const server = http.createServer(async (req, res) => {
    const city = await getCityFromRequest(req)
    console.log('city=' + city)
    if (city !== 'favicon.ico') {
        const ans = await temperatureService.get(city)
        res.setHeader('Access-Control-Allow-Origin', '*')
        res.setHeader('Access-Control-Allow-Headers', '*')
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.write(ans)
        res.end()
    }
})

async function getCityFromRequest(request) {
    const { headers, method, url } = request
    const strippedURL = url.substring(1, url.length) // remove the slash at url[0]
    const city = strippedURL.replace('%20', " ")
    return city
}

server.listen(port, (err) => {
    if (err) { return console.log('Error', err) }
    console.log(`Server listening at http://127.0.0.1:${port}`)
})

